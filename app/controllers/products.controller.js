const { sequelize, product } = require('../models');

exports.getProducts = async (req, res) => {
  try {
    const availableProducts = await product.findAll();
    return res.json(availableProducts);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: 'Something went wrong' });
  }
};

exports.getProduct = async (req, res) => {
  const { uuid } = req.params;
  try {
    const availableProduct = await product.findOne({
      where: { uuid },
    });
    return res.json(availableProduct);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: 'Something went wrong' });
  }
};
exports.deleteProduct = async (req, res) => {
  const { uuid } = req.params;
  try {
    const availableProduct = await product.findOne({
      where: { uuid },
    });
    await availableProduct.destroy();
    return res.json({ message: 'Product deleted !' });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: 'Something went wrong' });
  }
};

exports.updateProduct = async (req, res) => {
  const { uuid } = req.params;
  const {
    name, price, description, color, brand, category,
  } = req.body;
  try {
    const updatedProduct = await product.findOne({
      where: { uuid },
    });
    updatedProduct.name = name;
    updatedProduct.price = price;
    updatedProduct.description = description;
    updatedProduct.color = color;
    updatedProduct.brand = brand;
    updatedProduct.category = category;

    await updatedProduct.save();

    return res.json(updatedProduct);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: 'Something went wrong' });
  }
};
exports.createProduct = async (req,res) => {
  const {
    name, price, description, color, brand, category,
  } = req.body;
  try {
    const newproduct = await product.create({
      name, price, description, color, brand, category,
    });
    return res.json(newproduct);
  } catch (error) {
    console.log(error);
    // res.send('Data was not sent to database');
    return res.status(500).json(error);
  }
};
