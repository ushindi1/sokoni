/* eslint-disable no-console */
/* eslint-disable import/order */
/* eslint-disable no-unused-vars */

const productController = require('../controllers/products.controller');
const express = require('express');

const router = express.Router();

// End point for viewing all products existing on the platform.
router.get('/products', productController.getProducts);

// End point for viewing a product by its ID.
router.get('/products/:uuid', productController.getProduct);

// End point for deleting a product by its ID.
router.delete('/products/:uuid', productController.deleteProduct);

router.put('/products/:uuid', productController.updateProduct);

// End point for creating a product on the platform.
router.post('/products', productController.createProduct);

module.exports = router;
