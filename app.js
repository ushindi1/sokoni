/* eslint-disable prefer-destructuring */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
const express = require('express');
const { sequelize } = require('./app/models');

// Assigning app to express.
const app = express();
// Assigning a port number.
const port = 5000; 

const productRoutes = require('./app/routes/products.routes');
// Middleware
app.use(express.json());

// Project Routes.
app.use('/', productRoutes);

// Listening port.
app.listen(port, async () => {
  console.log(`Application running on http://localhost:${port}`);
  await sequelize.authenticate();
  console.log('Database Connected!');
});
